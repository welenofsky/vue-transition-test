import Vue from 'vue'
import App from './App.vue'
import Expander from './components/Expander.vue';

Vue.config.productionTip = false
Vue.component('expander', Expander);

new Vue({
  render: h => h(App),
}).$mount('#app')

